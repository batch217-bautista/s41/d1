const express = require("express");
const router = express.Router();
const Course = require("../models/Course.js");
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

/*
router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})
*/

// [ACTIVITY] verify first if user is admin before adding a course
// single course creation with authentication from user
router.post("/create", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => {
		res.send(resultFromController);
	})
})

// get all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => {
		res.send(resultFromController)
	})
})

// get all ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => {
		res.send(resultFromController)
	})
})

// get single/specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params.courseId).then(resultFromController =>
	{
		res.send(resultFromController)
	})
})

// Updating a single course
router.patch("/:courseId/update", auth.verify, (req, res) => {
	courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

// [ACTIVITY] code along
// archiving a single course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	})
})	

module.exports = router